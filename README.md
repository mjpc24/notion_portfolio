# Mark Cobarrubias

> I am a Software Developer as of now and currently under a training as a Flutter Developer. I am working on to be an efficient full-stack developer, be it in mobile or web. I am a former Network and System Administrator for almost 6 years. I am a goal oriented person and has the capability to do team work.

# Contact Information

LinkedIn: [linkedin.com/in/mark-jephilpan-cobarrubias-444b8981](http://linkedin.com/in/mark-jephilpan-cobarrubias-444b8981)

Website Portfolio: 

# Work Experience

## Programmer Trainee

FFUF Manila Flutter Development Trainee

- Trained to  use and develop mobile application using flutter framework

## Sr. IT Specialist

Global One Integrated Business Inc. May 4, 2015 to January 2021

- Maintaining and facilitating local mall server
- Designing, maintaining & trouble shooting Mall Network Infrastructure
- A team lead

## Skills

### Technical Skills

- Programming
- Software Engineering
- Network Infrastructures
- Virtualization
- System Administration

### Soft Skills

- A team lead
- Can speak and write Japanese level 5
- Can work and collaborate in a team

## Education

### BS Information Technology

Univerity of Rizal Sytem 

##